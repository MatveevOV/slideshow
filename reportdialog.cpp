#include "reportdialog.h"
#include "ui_reportdialog.h"

ReportDialog::ReportDialog(QVector<Slide> *topSlides, QVector<Slide> *bottomSlides, ThemeSelectDialog *parent) :
    QDialog(parent),
    ui(new Ui::ReportDialog)
{
    ui->setupUi(this);

    this->parent = parent;
    this->topSlides = topSlides;
    this->bottomSlides = bottomSlides;

    QFont newFont("Times New Roman", 12);
    this->setFont(newFont);

    this->setWindowModality(Qt::ApplicationModal);
    this->setWindowFlags(Qt::Dialog | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);
    this->setAttribute(Qt::WA_DeleteOnClose);

    User user = parent->getParent()->getParent()->getCurrentUser();
    userId = user.uuid;
    currentTheme = parent->getSelectedTheme();
    int numberOfQuestions = parent->getExamNumberOfQuestion();

    //заполнение данных студента
    ui->nameLabel->setText(QString("%1 %2 %3").arg(user.secondName).arg(user.firstName).arg(user.patronymic));
    ui->universityLabel->setText(user.university);
    ui->classLabel->setText(user.group);
    //---------------------------


    //получение данных о времени на вопросы по выбранной теме
    QSqlQuery query(parent->getParent()->getParent()->db);


    ui->questionTable->setRowCount(topSlides->length() + bottomSlides->length());
    ui->questionTable->setColumnCount(2);
    ui->questionTable->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
    ui->questionTable->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    ui->questionTable->horizontalHeader()->setVisible(true);
    ui->questionTable->verticalHeader()->setVisible(false);
    ui->questionTable->horizontalHeaderItem(0)->setFont(newFont);
    ui->questionTable->horizontalHeaderItem(1)->setFont(newFont);


    //ui->examTable->setRowCount(2*numberOfQuestions + 1);
    int examRowCount = 0;
    ui->examTable->setColumnCount(2);
    ui->examTable->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
    ui->examTable->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    ui->examTable->horizontalHeader()->setVisible(true);
    ui->examTable->verticalHeader()->setVisible(false);
    ui->examTable->horizontalHeaderItem(0)->setFont(newFont);
    ui->examTable->horizontalHeaderItem(1)->setFont(newFont);

    int numberOfRightAnswers = 0;

    for (int i = 0; i < topSlides->length(); i++)
    {
        //обучение
        QString selectTime = QString("SELECT time FROM questions WHERE userId = '%1' AND "
                                     "theme = '%2' AND "
                                     "questionId = %3").arg(userId).arg(currentTheme).arg(QString::number(topSlides->at(i).id));

        QString time;
        //qDebug() << selectTime << query.exec(selectTime) << query.lastError();
        if(query.exec(selectTime))
        {
            query.next();
            if (query.isValid())
                time = query.value(0).toString();
            //qDebug() << "time" << time <<  query.lastError();
        }

        QTableWidgetItem *c1 = new QTableWidgetItem(topSlides->at(i).imageTitle);
        c1->setFont(newFont);
        QTime realTime(0,0,0);
        QTableWidgetItem *c2 = new QTableWidgetItem(realTime.addSecs(time.toInt(0,10)).toString("hh:mm:ss"));
        c2->setFont(newFont);
        ui->questionTable->setItem(i, 0, c1);
        ui->questionTable->setItem(i, 1, c2);



        //экзамен
        QString selectExam = QString("SELECT inLastExam, isRight FROM questions WHERE userId = '%1' AND "
                                     "theme = '%2' AND "
                                     "questionId = %3").arg(userId).arg(currentTheme).arg(QString::number(topSlides->at(i).id));

        //qDebug() << selectTime << query.exec(selectTime) << query.lastError();
        bool inLastExam = false;
        bool isRight = false;
        if(query.exec(selectExam))
        {
            query.next();
            if (query.isValid())
            {
                inLastExam = query.value(0).toInt();
                isRight = query.value(1).toInt();



            }
        }


        if (inLastExam)
        {
            QTableWidgetItem *c1 = new QTableWidgetItem(topSlides->at(i).imageTitle);
            c1->setFont(newFont);
            QTableWidgetItem *c2 = new QTableWidgetItem();
            c2->setFont(newFont);
            if (isRight)
            {
                numberOfRightAnswers++;
                c2->setData(Qt::DisplayRole, "Верный ответ");
            }
            else
                c2->setData(Qt::DisplayRole, "Неверный ответ");
            examRowCount++;
            ui->examTable->setRowCount(examRowCount);
            ui->examTable->setItem(examRowCount-1, 0, c1);
            ui->examTable->setItem(examRowCount-1, 1, c2);
        }




    }
    for (int i = 0; i < bottomSlides->length(); i++)
    {
        QString selectTime = QString("SELECT time FROM questions WHERE userId = '%1' AND "
                                     "theme = '%2' AND "
                                     "questionId = %3").arg(userId).arg(currentTheme).arg(QString::number(bottomSlides->at(i).id));

        QString time;
        if(query.exec(selectTime))
        {
            query.next();
            if (query.isValid())
                time = query.value(0).toString();
            //qDebug() << "time" << time <<  query.lastError();
        }
        QTableWidgetItem *c1 = new QTableWidgetItem(bottomSlides->at(i).imageTitle);
        c1->setFont(newFont);
        QTime realTime(0,0,0);
        QTableWidgetItem *c2 = new QTableWidgetItem(realTime.addSecs(time.toInt(0,10)).toString("hh:mm:ss"));
        c2->setFont(newFont);
        ui->questionTable->setItem(i + topSlides->length(), 0, c1);
        ui->questionTable->setItem(i + topSlides->length(), 1, c2);


        //экзамен
        QString selectExam = QString("SELECT inLastExam, isRight FROM questions WHERE userId = '%1' AND "
                                     "theme = '%2' AND "
                                     "questionId = %3").arg(userId).arg(currentTheme).arg(QString::number(bottomSlides->at(i).id));

        bool inLastExam = false;
        bool isRight = false;
        if(query.exec(selectExam))
        {
            query.next();
            //qDebug() << query.lastQuery();
            if (query.isValid())
            {
                inLastExam = query.value(0).toInt();
                isRight = query.value(1).toInt();


            }
        }


        if (inLastExam)
        {
            QTableWidgetItem *c1 = new QTableWidgetItem(bottomSlides->at(i).imageTitle);
            QTableWidgetItem *c2 = new QTableWidgetItem();
            if (isRight)
            {
                numberOfRightAnswers++;
                c2->setData(Qt::DisplayRole, "Верный ответ");
            }
            else
                c2->setData(Qt::DisplayRole, "Неверный ответ");
            examRowCount++;
            ui->examTable->setRowCount(examRowCount);
            ui->examTable->setItem(examRowCount-1, 0, c1);
            ui->examTable->setItem(examRowCount-1, 1, c2);
        }

    }

    QTableWidgetItem *c1 = new QTableWidgetItem("Результат");
    c1->setFont(newFont);
    c1->setData(Qt::BackgroundRole,QColor(0,255,255));
    QTableWidgetItem *c2 = new QTableWidgetItem(QString("%1/%2 : %3%").arg(numberOfRightAnswers)
                                                                      .arg(2*numberOfQuestions)
                                                                      .arg(100*numberOfRightAnswers/(2*numberOfQuestions)));
    c2->setData(Qt::BackgroundRole,QColor(0,255,255));
    c2->setFont(newFont);

    examRowCount++;
    ui->examTable->setRowCount(examRowCount);
    ui->examTable->setItem(examRowCount-1, 0, c1);
    ui->examTable->setItem(examRowCount-1, 1, c2);




}

ReportDialog::~ReportDialog()
{
    delete ui;
}

void ReportDialog::reject()
{
    done(Rejected);
}

void ReportDialog::on_backButton_clicked()
{
    this->close();
}
