#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QProcess>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QUuid>
#include <QSqlError>

#include "modedialog.h"
#include "structures.h"


class ModeDialog;

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();
    QSqlDatabase db;



    User getCurrentUser() const;

private:
    Ui::LoginDialog *ui;
    ModeDialog *modeDialog;
    User currentUser;

private slots:
    void accept();
    void reject();
};

#endif // LOGINDIALOG_H
