#include "trainingdialog.h"
#include "ui_trainingdialog.h"

TrainingDialog::TrainingDialog(QVector<Slide> *topSlides,
                               QVector<Slide> *bottomSlides,
                               ThemeSelectDialog *parent) :
    ui(new Ui::TrainingDialog)
{
    ui->setupUi(this);
    this->parent = parent;
    this->topSlides = topSlides;
    this->bottomSlides = bottomSlides;

    this->setWindowModality(Qt::ApplicationModal);
    //this->setWindowFlags(Qt::Dialog | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);
    this->setWindowFlags(Qt::ToolTip | Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_DeleteOnClose);

    userId = parent->getParent()->getParent()->getCurrentUser().uuid;
    currentTheme = parent->getSelectedTheme();
    ui->currentThemeLabel->setText(currentTheme);
    //создание таблицы, если нужно
    QSqlQuery query(parent->getParent()->getParent()->db);

    //qDebug() << "drop" << query.exec("DROP TABLE questions");


    QString str =     "create table if not exists questions "
              "(id INTEGER PRIMARY KEY ASC, "
              "theme text not null, "
              "userId text not null, "
              "questionId integer not null, "
              "time integer not null, "
              "inLastExam integer not null, "
              "isRight integer not null)";



    qDebug() << "create table questions" << query.exec(str);
    //-------


    currentTopSlide = 0;
    topTime = QTime::currentTime();
    currentBottomSlide = 0;
    bottomTime = QTime::currentTime();

    if (!topSlides->isEmpty())
        setTopSlide(currentTopSlide);
    if (!bottomSlides->isEmpty())
        setBottomSlide(currentBottomSlide);


}

TrainingDialog::~TrainingDialog()
{
    delete ui;
}

void TrainingDialog::reject()
{
    done(Rejected);
}

void TrainingDialog::setTopSlide(int i)
{

    QImage img(topSlides->at(i).imagePath);
    qDebug() << "slide" <<topSlides->at(i).imagePath << img;

    QImage scaledImage = img.scaled(ui->topEye->size(),
                                    Qt::IgnoreAspectRatio,
                                    Qt::SmoothTransformation);
    ui->topEye->setPixmap(QPixmap::fromImage(scaledImage));

    img = QImage(topSlides->at(i).processedImagePath);
    scaledImage = img.scaled(ui->topEyeProcessed->size(),
                                    Qt::IgnoreAspectRatio,
                                    Qt::SmoothTransformation);
    ui->topEyeProcessed->setPixmap(QPixmap::fromImage(scaledImage));

    ui->topEyeTitle->setText(topSlides->at(i).imageTitle);
    ui->topEyeText->setText(topSlides->at(i).imageText);

    ui->currentTopSlide->setText(QString("Слайд %1 из %2").arg(currentTopSlide + 1).arg(topSlides->length()));

    topEyeChanged(i);


}

void TrainingDialog::setBottomSlide(int i)
{
    QImage img(bottomSlides->at(i).imagePath);
    QImage scaledImage = img.scaled(ui->bottomEye->size(),
                                    Qt::IgnoreAspectRatio,
                                    Qt::SmoothTransformation);
    ui->bottomEye->setPixmap(QPixmap::fromImage(scaledImage));

    img = QImage(bottomSlides->at(i).processedImagePath);
    scaledImage = img.scaled(ui->bottomEyeProcessed->size(),
                                    Qt::IgnoreAspectRatio,
                                    Qt::SmoothTransformation);
    ui->bottomEyeProcessed->setPixmap(QPixmap::fromImage(scaledImage));

    ui->bottomEyeTitle->setText(bottomSlides->at(i).imageTitle);
    ui->bottomEyeText->setText(bottomSlides->at(i).imageText);

    ui->currentBottomSlide->setText(QString("Слайд %1 из %2").arg(currentBottomSlide + 1).arg(bottomSlides->length()));

    bottomEyeChanged(i);
}

bool TrainingDialog::updateSlideInDb(int slideNumber, int secs)
{
    bool isExec = false;
    QSqlQuery query(parent->getParent()->getParent()->db);

    //запрос времени по данному слайду из таблицы
    QString selectTime = QString("SELECT time FROM questions WHERE userId = '%1' AND "
                                 "theme = '%2' AND "
                                 "questionId = %3").arg(userId).arg(currentTheme).arg(QString::number(slideNumber));

    QString time;
    if(query.exec(selectTime))
    {
        query.next();
        time = query.value(0).toString();
    }
    //-------------------------------------------

    //update времени, если уже было записано,
    //insert если не было

    if (time.isEmpty())
    {
        QString str = QString("INSERT INTO questions "
                "(theme, "
                "userId, "
                "questionId, "
                "time, "
                "inLastExam, "
                "isRight) "
                "VALUES ('%1', '%2', %3, %4, %5, %6)").arg(currentTheme)
                                                  .arg(userId)
                                                  .arg(QString::number(slideNumber))
                                                  .arg(QString::number(time.toInt(0,10) + secs))
                                                  .arg(0)
                                                  .arg(0);
        isExec = query.exec(str);
        qDebug() << "insert" << isExec << str << query.lastError();
    }
    else
    {

        QString str = QString("UPDATE questions "
                       "SET time = %1 "
                       "WHERE userId = '%2' AND "
                       "theme = '%3' AND "
                       "questionId = %4").arg(QString::number(time.toInt(0,10) + secs))
                                         .arg(userId)
                                         .arg(currentTheme)
                                         .arg(QString::number(slideNumber));


        isExec = query.exec(str);
        qDebug() << "update" << str  << isExec << query.lastError();
    }

    //-------------

    return isExec;
}

void TrainingDialog::resizeEvent(QResizeEvent *event)
{
    if (!topSlides->isEmpty())
        setTopSlide(currentTopSlide);
    if (!bottomSlides->isEmpty())
        setBottomSlide(currentBottomSlide);

    QDialog::resizeEvent(event);
}

void TrainingDialog::mousePressEvent(QMouseEvent *e)
{
    QString imagePath;
    QString processedImagePath;

    QWidget *widget = childAt(e->pos());
    if (widget != 0 &&
            (widget->objectName() == "topEye" || widget->objectName() == "topEyeProcessed"))
    {
        imagePath = topSlides->at(currentTopSlide).imagePath;
        processedImagePath = topSlides->at(currentTopSlide).processedImagePath;

    }

    if (widget != 0 &&
            (widget->objectName() == "bottomEye" || widget->objectName() == "bottomEyeProcessed"))
    {
        imagePath = bottomSlides->at(currentBottomSlide).imagePath;
        processedImagePath = bottomSlides->at(currentBottomSlide).processedImagePath;

    }


        if (!imagePath.isEmpty() && !processedImagePath.isEmpty())
        {
            QDialog *imageDialog = new QDialog();
            imageDialog->setMinimumSize(1260,600);
            imageDialog->setMaximumSize(1260,600);

            QHBoxLayout *hbox = new QHBoxLayout;
            QLabel *imageLabel = new QLabel;            
            QLabel *processedImageLabel = new QLabel;


            QImage img1(imagePath);
            QImage scaledImage1 = img1.scaled(imageDialog->minimumSize(),
                                            Qt::KeepAspectRatio,
                                            Qt::SmoothTransformation);

            imageLabel->setPixmap(QPixmap::fromImage(scaledImage1));
            hbox->addWidget(imageLabel);

            QImage img2(processedImagePath);
            QImage scaledImage2 = img2.scaled(imageDialog->minimumSize(),
                                            Qt::KeepAspectRatio,
                                            Qt::SmoothTransformation);

            processedImageLabel->setPixmap(QPixmap::fromImage(scaledImage2));
            hbox->addWidget(processedImageLabel);

            imageDialog->setLayout(hbox);

            imageDialog->setWindowModality(Qt::ApplicationModal);
            imageDialog->setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);
            imageDialog->setAttribute(Qt::WA_DeleteOnClose);

            imageDialog->show();
        }


    QDialog::mousePressEvent(e);


}

void TrainingDialog::on_backButtonTop_clicked()
{
    int secs = topTime.secsTo(QTime::currentTime());
    updateSlideInDb(topSlides->at(currentTopSlide).id, secs);

    currentTopSlide--;
    if (currentTopSlide < 0)
        currentTopSlide = topSlides->length() - 1;
    setTopSlide(currentTopSlide);

    topTime = QTime::currentTime();

}

void TrainingDialog::on_forwardButtonTop_clicked()
{

    int secs = topTime.secsTo(QTime::currentTime());
    updateSlideInDb(topSlides->at(currentTopSlide).id, secs);

    currentTopSlide++;
    if (currentTopSlide == topSlides->length())
        currentTopSlide = 0;
    setTopSlide(currentTopSlide);

    topTime = QTime::currentTime();


}

void TrainingDialog::on_backButtonBottom_clicked()
{
    int secs = bottomTime.secsTo(QTime::currentTime());
    updateSlideInDb(bottomSlides->at(currentBottomSlide).id, secs);

    currentBottomSlide--;
    if (currentBottomSlide < 0)
        currentBottomSlide = bottomSlides->length() - 1;
    setBottomSlide(currentBottomSlide);

    bottomTime = QTime::currentTime();


}

void TrainingDialog::on_forwardButtonBottom_clicked()
{
    int secs = bottomTime.secsTo(QTime::currentTime());
    updateSlideInDb(bottomSlides->at(currentBottomSlide).id, secs);

    currentBottomSlide++;
    if (currentBottomSlide == bottomSlides->length())
        currentBottomSlide = 0;
    setBottomSlide(currentBottomSlide);

    bottomTime = QTime::currentTime();


}

void TrainingDialog::on_endButton_clicked()
{
    int secs = topTime.secsTo(QTime::currentTime());
    updateSlideInDb(topSlides->at(currentTopSlide).id, secs);

    secs = bottomTime.secsTo(QTime::currentTime());
    updateSlideInDb(bottomSlides->at(currentBottomSlide).id, secs);

    this->close();
}
