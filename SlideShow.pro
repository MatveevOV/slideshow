#-------------------------------------------------
#
# Project created by QtCreator 2017-02-13T09:57:00
#
#-------------------------------------------------

QT       += core gui sql xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = OphtalmologicExamination
TEMPLATE = app


SOURCES += main.cpp\
    logindialog.cpp \
    modedialog.cpp \
    trainingdialog.cpp \
    examdialog.cpp \
    reportdialog.cpp \
    themeselectdialog.cpp \
    dummydialog.cpp

HEADERS  += \
    logindialog.h \
    modedialog.h \
    trainingdialog.h \
    examdialog.h \
    reportdialog.h \
    themeselectdialog.h \
    structures.h \
    dummydialog.h

FORMS    += \
    logindialog.ui \
    modedialog.ui \
    trainingdialog.ui \
    examdialog.ui \
    reportdialog.ui \
    dummydialog.ui

RESOURCES += \
    resources.qrc

MAJOR_VER = 0
MINOR_VER = 1
PATCH_VER = 0

RC_ICONS = "appIcon.ico"
VERSION = $${MAJOR_VER}.$${MINOR_VER}.$${PATCH_VER}   # major.minor.patch
QMAKE_TARGET_PRODUCT = OphtalmologicExamination
QMAKE_TARGET_DESCRIPTION = OphtalmologicExamination
