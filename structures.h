#ifndef STRUCTURES
#define STRUCTURES

#include <QString>


struct User{
    QString uuid;
    QString secondName;
    QString firstName;
    QString patronymic;
    QString university;
    QString group;
};

struct Slide
{
   int id;
   QString imagePath;
   QString imageTitle;
   QString imageText;
   QString processedImagePath;

   QVector<QString> examPossibleAnswers;
   QString examRightAnswer;

};

#endif // STRUCTURES

