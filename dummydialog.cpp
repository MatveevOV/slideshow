#include "dummydialog.h"
#include "ui_dummydialog.h"

DummyDialog::DummyDialog(QVector<Slide> *topSlides,
                         QVector<Slide> *bottomSlides,
                         ThemeSelectDialog *parent) :
    QDialog(parent),
    ui(new Ui::DummyDialog)
{
    ui->setupUi(this);
    this->parent = parent;
    this->topSlides = topSlides;
    this->bottomSlides = bottomSlides;


    this->setWindowModality(Qt::NonModal);
    this->setWindowFlags(Qt::ToolTip | Qt::FramelessWindowHint);

    currentTopSlide = 0;
    topTime = QTime::currentTime();
    currentBottomSlide = 0;
    bottomTime = QTime::currentTime();

    if (!topSlides->isEmpty())
        setTopSlide(currentTopSlide);
    if (!bottomSlides->isEmpty())
        setBottomSlide(currentBottomSlide);

}

DummyDialog::~DummyDialog()
{
    delete ui;
}

void DummyDialog::setTopSlide(int i)
{
    QImage img(topSlides->at(i).imagePath);
    QImage scaledImage = img.scaled(ui->topEye->size(),
                                    Qt::IgnoreAspectRatio,
                                    Qt::SmoothTransformation);
    ui->topEye->setPixmap(QPixmap::fromImage(scaledImage));


}

void DummyDialog::setBottomSlide(int i)
{
    QImage img(bottomSlides->at(i).imagePath);
    QImage scaledImage = img.scaled(ui->bottomEye->size(),
                                    Qt::IgnoreAspectRatio,
                                    Qt::SmoothTransformation);
    ui->bottomEye->setPixmap(QPixmap::fromImage(scaledImage));

}

void DummyDialog::reject()
{
    done(Rejected);
}
