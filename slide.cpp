#include "slide.h"
#include "ui_slide.h"

Slide::Slide(QString dirName, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Slide)
{
    ui->setupUi(this);
    this->setGeometry(QApplication::desktop()->geometry());
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Popup);
    QDir dir(dirName);
    showedFiles = dir.entryInfoList(QDir::Files);

    if (!showedFiles.isEmpty())
    {
        currentPic = -1;
        pixCount = showedFiles.length();
        QTimer::singleShot(5,this,&Slide::on_rightButton_clicked);
    }

    ui->leftButton->setShortcut(QKeySequence(Qt::Key_Left));
    ui->leftButton->setFocusPolicy(Qt::NoFocus);
    ui->rightButton->setShortcut(QKeySequence(Qt::Key_Right));
    ui->rightButton->setFocusPolicy(Qt::NoFocus);
    ui->leftButton->hide();
    ui->rightButton->hide();

    ui->verticalLayout->setAlignment(Qt::AlignCenter);

}

Slide::~Slide()
{
    delete ui;
}

void Slide::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Left)
    {
        on_leftButton_clicked();
    }
    if (event->key() == Qt::Key_Right)
    {
        on_rightButton_clicked();
    }
    if (event->key() == Qt::Key_Escape)
    {
        close();
    }

}

void Slide::on_leftButton_clicked()
{
    currentPic--;
    if (currentPic < 0)
        currentPic = pixCount - 1;

        QImage img(showedFiles.at(currentPic).filePath());
        QImage scaledImage = img.scaled(ui->pix->size(),
                                        Qt::KeepAspectRatio,
                                        Qt::SmoothTransformation);
        ui->pix->setPixmap(QPixmap::fromImage(scaledImage));
        ui->pix->setAlignment(Qt::AlignCenter);

}

void Slide::on_rightButton_clicked()
{
    currentPic++;
    if (currentPic == pixCount)
        currentPic = 0;

        QImage img(showedFiles.at(currentPic).filePath());
        QImage scaledImage = img.scaled(ui->pix->size(),
                                        Qt::KeepAspectRatio,
                                        Qt::SmoothTransformation);
        ui->pix->setPixmap(QPixmap::fromImage(scaledImage));
        ui->pix->setAlignment(Qt::AlignCenter);

}
