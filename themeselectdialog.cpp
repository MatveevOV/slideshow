#include "themeselectdialog.h"

ThemeSelectDialog::ThemeSelectDialog(QStringList themes, int mode, ModeDialog *parent)
{
    this->parent = parent;
    setWindowFlags(Qt::Dialog|Qt::WindowCloseButtonHint);
    setGeometry(this->parent->geometry());
    //setWindowTitle("Выбор темы");

    QFont newFont("Times New Roman", 12);
    this->setFont(newFont);

    this->mode = mode;
    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->addStretch(1);
    for (int i = 0; i < themes.length(); i++)
    {
        QPushButton *themeButton = new QPushButton(themes.at(i));
        themeButton->setMinimumHeight(40);
        connect(themeButton, &QPushButton::released, this, &ThemeSelectDialog::sendSelectedTheme);
        vbox->addWidget(themeButton);
    }

    vbox->addStretch(1);
    QPushButton *backButton = new QPushButton("Назад");
    backButton->setMinimumHeight(40);
    vbox->addWidget(backButton);
    this->setLayout(vbox);
    connect(backButton, &QPushButton::released, this, &ThemeSelectDialog::toModeDialog);
}

ThemeSelectDialog::~ThemeSelectDialog()
{
}

int ThemeSelectDialog::getExamNumberOfQuestion() const
{
    return examNumberOfQuestion;
}

bool ThemeSelectDialog::themeParse(QString dir)
{
    QFile description;

    description.setFileName(QString(dir + "/description.xml"));
        topSlides.clear();
        bottomSlides.clear();
        if (description.open(QFile::ReadOnly | QFile::Text)){
            Rxml.setDevice(&description);

            while(!Rxml.atEnd())
            {

                Rxml.readNext();
                if(Rxml.isStartElement() && Rxml.name() == "General")
                {

                    qDebug() << "<<< START parse >>>";

                }
                if(Rxml.isStartElement() && Rxml.name() == "NumberOfQuestions")
                {
                    examNumberOfQuestion =  Rxml.readElementText().toInt();

                }

                if(Rxml.isStartElement() && (Rxml.name() == "UpPart"))
                {
                    topSlides.append(slideParse());
                }

                if(Rxml.isStartElement() && (Rxml.name() == "DownPart"))
                {
                    bottomSlides.append(slideParse());
                }

                if(Rxml.isEndElement() && Rxml.name() == "General")
                {

                    qDebug() << "<<< END parse >>>";
                    Rxml.readNext();
                    break;

                }

            }

            description.close();
            return true;
        } else {
            description.close();
            return false;
        }
}

Slide ThemeSelectDialog::slideParse()
{
    Slide newSlide;
    while(!Rxml.atEnd())
    {
        Rxml.readNext();
        if(Rxml.isStartElement())
        {
            if(Rxml.name() == "Number")
            {
                newSlide.id = Rxml.readElementText().toInt(0,10);

            }
            if(Rxml.name() == "OriginImage")
            {
                newSlide.imagePath = QString("./data/" + selectedTheme + "/" +Rxml.readElementText());
            }
            if(Rxml.name() == "ProcessedImage")
            {
                newSlide.processedImagePath = QString("./data/" + selectedTheme + "/" +Rxml.readElementText());

            }
            if(Rxml.name() == "SlideTitle")
            {
                newSlide.imageTitle = Rxml.readElementText();

            }
            if(Rxml.name() == "SlideText")
            {
                newSlide.imageText = Rxml.readElementText();

            }
            if(Rxml.name() == "ExamPossibleAnswers")
            {
                QVector<QString> answers;
                while(!((Rxml.isEndElement()) && (Rxml.name() == "ExamPossibleAnswers")))
                {
                    Rxml.readNext();
                    if (Rxml.name() == "Answer")
                    {
                        answers.append(Rxml.readElementText());
                    }
                }
                newSlide.examPossibleAnswers = answers;
            }

            if(Rxml.name() == "ExamRightAnswer")
            {
                newSlide.examRightAnswer = Rxml.readElementText();

            }

        }
        if(Rxml.isEndElement())
        {
            if(Rxml.name() == "UpPart" || Rxml.name() == "DownPart")
            {
                Rxml.readNext();
                break;
            }
        }
    }

    return newSlide;
}

void ThemeSelectDialog::toModeDialog()
{
    parent->show();
    this->close();
}

void ThemeSelectDialog::sendSelectedTheme()
{
    QPushButton *btn = qobject_cast <QPushButton*>(sender());
    selectedTheme = btn->text();

    if (themeParse(QString("./data/" + selectedTheme)))
    {
        switch (mode) {
        case ModeDialog::TRAINING:
        {

            TrainingDialog *d = new TrainingDialog(&topSlides, &bottomSlides, this);
            QRect screenres = QApplication::desktop()->screenGeometry(0);
            QRect screenres2 = QApplication::desktop()->screenGeometry(1);

            d->move(QPoint(screenres.x(), screenres.y()));
            d->setGeometry(screenres);
            d->showMaximized();

            if (screenres2.width() < 1360)
            {
                DummyDialog *dummy = new DummyDialog(&topSlides, &bottomSlides, this);
                dummy->move(QPoint(screenres2.x(), screenres2.y()));
                dummy->show();

                connect(d,&TrainingDialog::topEyeChanged, dummy, &DummyDialog::setTopSlide);
                connect(d,&TrainingDialog::bottomEyeChanged, dummy, &DummyDialog::setBottomSlide);
                connect(d, &TrainingDialog::rejected, dummy, &DummyDialog::reject);
            }

        }
            break;
        case ModeDialog::EXAM:
        {          

            ExamDialog *d = new ExamDialog(&topSlides, &bottomSlides, this);
            QRect screenres = QApplication::desktop()->screenGeometry(0);
            QRect screenres2 = QApplication::desktop()->screenGeometry(1);

            d->move(QPoint(screenres.x(), screenres.y()));
            d->setGeometry(screenres);
            d->showMaximized();

            if (screenres2.width() < 1360)
            {
                DummyDialog *dummy = new DummyDialog(&topSlides, &bottomSlides, this);
                dummy->move(QPoint(screenres2.x(), screenres2.y()));
                dummy->show();
                connect(d,&ExamDialog::topEyeChanged, dummy, &DummyDialog::setTopSlide);
                connect(d,&ExamDialog::bottomEyeChanged, dummy, &DummyDialog::setBottomSlide);
                connect(d, &ExamDialog::rejected, dummy, &DummyDialog::reject);
            }

        }
            break;
        case ModeDialog::REPORT:
        {
            ReportDialog *d = new ReportDialog(&topSlides, &bottomSlides, this);
            //d->move(QPoint(screenres.x(), screenres.y()));
            d->showMaximized();

        }
            break;
        default:
            break;
        }

    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Не удалось получить данные по теме");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }

}

QString ThemeSelectDialog::getSelectedTheme() const
{
    return selectedTheme;
}

ModeDialog *ThemeSelectDialog::getParent() const
{
    return parent;
}

void ThemeSelectDialog::reject()
{
    done(Rejected);
}

void ThemeSelectDialog::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
    {
        toModeDialog();
    }

}
