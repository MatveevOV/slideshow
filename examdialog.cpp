#include "examdialog.h"
#include "ui_examdialog.h"

ExamDialog::ExamDialog(QVector<Slide> *topSlides,
                       QVector<Slide> *bottomSlides,
                       ThemeSelectDialog *parent) :
    ui(new Ui::ExamDialog)
{
    ui->setupUi(this);
    this->parent = parent;
    this->topSlides = topSlides;
    this->bottomSlides = bottomSlides;

    userId = parent->getParent()->getParent()->getCurrentUser().uuid;
    currentTheme = parent->getSelectedTheme();
    ui->currentThemeLabel->setText(currentTheme);
    numberOfQuestions = parent->getExamNumberOfQuestion();

    this->setWindowModality(Qt::ApplicationModal);
    //this->setWindowFlags(Qt::Dialog | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);
    this->setWindowFlags(Qt::ToolTip | Qt::FramelessWindowHint );

    this->setAttribute(Qt::WA_DeleteOnClose);

    ui->topExamWidget->setLayout(&topVBox);
    ui->bottomExamWidget->setLayout(&bottomVBox);

    topExamQuestions = getTopSlidesForExam();
    bottomExamQuestions = getBottomSlidesForExam();

    currentTopSlide = topExamQuestions.begin();
    currentBottomSlide = bottomExamQuestions.begin();

    ui->backButton->setEnabled(false);
    currentQuestion = 1;
    ui->currentQuestionLabel->setText(QString("Вопрос %1 из %2").arg(currentQuestion).arg(numberOfQuestions));


    topVBox.setMargin(100);
    bottomVBox.setMargin(100);



}

ExamDialog::~ExamDialog()
{
    delete ui;
}

void ExamDialog::reject()
{
    done(Rejected);
}

void ExamDialog::setSlide(int t, int b)
{

    QImage img(topSlides->at(t).imagePath);
    QImage scaledImage = img.scaled(ui->topEye->size(),
                                    Qt::KeepAspectRatio,
                                    Qt::SmoothTransformation);
    ui->topEye->setPixmap(QPixmap::fromImage(scaledImage));

    img = QImage(bottomSlides->at(b).imagePath);
    scaledImage = img.scaled(ui->bottomEye->size(),
                                    Qt::KeepAspectRatio,
                                    Qt::SmoothTransformation);
    ui->bottomEye->setPixmap(QPixmap::fromImage(scaledImage));

    emit topEyeChanged(currentTopSlide.key());
    emit bottomEyeChanged(currentBottomSlide.key());

    qDeleteAll(this->findChildren<QRadioButton*>("", Qt::FindChildrenRecursively));

    QButtonGroup topRadio;
    QVector<QString> answers = topSlides->at(t).examPossibleAnswers;
    QVector<QString> randomAnswers;
    int n = answers.length();
    while (randomAnswers.size() < n)
    {
        int r = qrand();
        int randomElement = r % answers.size();
        int pos = randomAnswers.indexOf(answers.at(randomElement));
        if (pos == -1)
            randomAnswers.append(answers.at(randomElement));

    }

    foreach (const QString &value, randomAnswers)
    {
        QRadioButton *newRadio = new QRadioButton(value);
        topRadio.addButton(newRadio);
        topVBox.addWidget(newRadio);
    }
    topRadio.button(-2)->setChecked(false);


    QButtonGroup bottomRadio;
    answers = bottomSlides->at(b).examPossibleAnswers;
    randomAnswers.clear();
    n = answers.length();
    while (randomAnswers.size() < n)
    {
        int r = qrand();
        int randomElement = r % answers.size();
        int pos = randomAnswers.indexOf(answers.at(randomElement));
        if (pos == -1)
            randomAnswers.append(answers.at(randomElement));

    }

    foreach (const QString &value, randomAnswers)
    {
        QRadioButton *newRadio = new QRadioButton(value);
        bottomRadio.addButton(newRadio);
        bottomVBox.addWidget(newRadio);
    }
    bottomRadio.button(-2)->setChecked(false);

}

void ExamDialog::resizeEvent(QResizeEvent *event)
{
    if (!topExamQuestions.isEmpty() && !bottomExamQuestions.isEmpty())
        setSlide(currentTopSlide.key(), currentBottomSlide.key());

    QDialog::resizeEvent(event);
}

void ExamDialog::on_backButton_clicked()
{

    QList<QRadioButton*> answers = ui->topExamWidget->findChildren<QRadioButton*>("", Qt::FindDirectChildrenOnly);
    for (int i = 0; i < answers.length(); i++)
    {
      if (answers.at(i)->isChecked())
          topExamQuestions.insert(currentTopSlide.key(),checkTopSlide(answers.at(i)->text(), currentTopSlide.key()));

    }
    qDeleteAll(answers);
    answers = ui->bottomExamWidget->findChildren<QRadioButton*>("", Qt::FindDirectChildrenOnly);
    for (int i = 0; i < answers.length(); i++)
    {
      if (answers.at(i)->isChecked())
          bottomExamQuestions.insert(currentBottomSlide.key(),checkBottomSlide(answers.at(i)->text(), currentBottomSlide.key()));

    }
    qDeleteAll(answers);

    currentTopSlide--;
    currentBottomSlide--;

    setSlide(currentTopSlide.key(), currentBottomSlide.key());

    ui->forwardButton->setEnabled(true);
    if (currentTopSlide == topExamQuestions.begin())
        ui->backButton->setEnabled(false);
    else
        ui->backButton->setEnabled(true);

    currentQuestion--;
    ui->currentQuestionLabel->setText(QString("Вопрос %1 из %2").arg(currentQuestion).arg(numberOfQuestions));

}

void ExamDialog::on_forwardButton_clicked()
{

    QList<QRadioButton*> answers = ui->topExamWidget->findChildren<QRadioButton*>("", Qt::FindDirectChildrenOnly);
    for (int i = 0; i < answers.length(); i++)
    {
      if (answers.at(i)->isChecked())
          topExamQuestions.insert(currentTopSlide.key(),checkTopSlide(answers.at(i)->text(), currentTopSlide.key()));

    }
    qDeleteAll(answers);
    answers = ui->bottomExamWidget->findChildren<QRadioButton*>("", Qt::FindDirectChildrenOnly);
    for (int i = 0; i < answers.length(); i++)
    {
      if (answers.at(i)->isChecked())
          bottomExamQuestions.insert(currentBottomSlide.key(),checkBottomSlide(answers.at(i)->text(), currentBottomSlide.key()));

    }
    qDeleteAll(answers);


    currentTopSlide++;
    currentBottomSlide++;

    setSlide(currentTopSlide.key(), currentBottomSlide.key());

    ui->backButton->setEnabled(true);
    if (currentTopSlide + 1 == topExamQuestions.end())
        ui->forwardButton->setEnabled(false);
    else
        ui->forwardButton->setEnabled(true);

    currentQuestion++;
    ui->currentQuestionLabel->setText(QString("Вопрос %1 из %2").arg(currentQuestion).arg(numberOfQuestions));
}

QMap<int, bool> ExamDialog::getTopSlidesForExam()
{
    QMultiMap <int, int> map;
    QSqlQuery query(parent->getParent()->getParent()->db);
    for (int i = 0; i < topSlides->length(); i++)
    {
        QString selectTime = QString("SELECT time FROM questions WHERE userId = '%1' AND "
                                     "theme = '%2' AND "
                                     "questionId = %3").arg(userId).arg(currentTheme).arg(QString::number(topSlides->at(i).id));

        QString time;
        if(query.exec(selectTime))
        {
            query.next();
            if (query.isValid())
                time = query.value(0).toString();
        }
        map.insert(time.toInt(0,10),i);
    }

    QMap <int, bool> examMap;

    auto iter = map.begin();
    int question = 0;
    for (; iter != map.end(); ++iter)
    {
        if (question == numberOfQuestions)
            break;
        examMap.insert(iter.value(),false);
        question++;
    }
    return examMap;
}

QMap<int, bool> ExamDialog::getBottomSlidesForExam()
{
    QMultiMap <int, int> map;
    QSqlQuery query(parent->getParent()->getParent()->db);
    for (int i = 0; i < bottomSlides->length(); i++)
    {
        QString selectTime = QString("SELECT time FROM questions WHERE userId = '%1' AND "
                                     "theme = '%2' AND "
                                     "questionId = %3").arg(userId).arg(currentTheme).arg(QString::number(bottomSlides->at(i).id));

        QString time;
        if(query.exec(selectTime))
        {
            query.next();
            if (query.isValid())
                time = query.value(0).toString();
        }

        map.insert(time.toInt(0,10),i);
    }

    QMap <int, bool> examMap;

    auto iter = map.begin();
    int question = 0;
    for (; iter != map.end(); ++iter)
    {
        if (question == numberOfQuestions)
            break;
        examMap.insert(iter.value(),false);
        question++;
    }

    return examMap;
}

bool ExamDialog::checkTopSlide(QString answer, int slideNumber)
{
    QByteArray ba;
    ba.append(answer);
    QString hash = QCryptographicHash::hash(ba,QCryptographicHash::Md5).toHex();


    if (hash == topSlides->at(slideNumber).examRightAnswer)
        return true;
    else
        return false;

}

bool ExamDialog::checkBottomSlide(QString answer, int slideNumber)
{
    QByteArray ba;
    ba.append(answer);
    QString hash = QCryptographicHash::hash(ba,QCryptographicHash::Md5).toHex();
    if (hash == bottomSlides->at(slideNumber).examRightAnswer)
        return true;
    else
        return false;

}

void ExamDialog::on_endButton_clicked()
{
    //проверить последний вопрос
    QList<QRadioButton*> answers = ui->topExamWidget->findChildren<QRadioButton*>("", Qt::FindDirectChildrenOnly);
    for (int i = 0; i < answers.length(); i++)
    {
      if (answers.at(i)->isChecked())
          topExamQuestions.insert(currentTopSlide.key(),checkTopSlide(answers.at(i)->text(), currentTopSlide.key()));

    }
    answers = ui->bottomExamWidget->findChildren<QRadioButton*>("", Qt::FindDirectChildrenOnly);
    for (int i = 0; i < answers.length(); i++)
    {
      if (answers.at(i)->isChecked())
          bottomExamQuestions.insert(currentBottomSlide.key(),checkBottomSlide(answers.at(i)->text(), currentBottomSlide.key()));

    }

    QSqlQuery query(parent->getParent()->getParent()->db);

    for (int i = 0; i < topSlides->length(); i++)
    {
        int slideNumber = topSlides->at(i).id;
        bool isRight = false;
        bool inLastExam = false;
        if (topExamQuestions.contains(i))
        {
            inLastExam = true;
            isRight = topExamQuestions.value(i);
        }


        //запрос по данному слайду из таблицы
        QString select = QString("SELECT time FROM questions WHERE userId = '%1' AND "
                                     "theme = '%2' AND "
                                     "questionId = %3").arg(userId).arg(currentTheme).arg(QString::number(slideNumber));
        QString insert = QString("INSERT INTO questions "
                "(theme, "
                "userId, "
                "questionId, "
                "time, "
                "inLastExam, "
                "isRight) "
                "VALUES ('%1', '%2', %3, %4, %5, %6)").arg(currentTheme)
                                                      .arg(userId)
                                                      .arg(QString::number(slideNumber))
                                                      .arg(QString::number(0))
                                                      .arg(inLastExam)
                                                      .arg(isRight);
        QString update = QString("UPDATE questions "
                       "SET isRight = %1, "
                       "inLastExam = %2 "
                       "WHERE userId = '%3' AND "
                       "theme = '%4' AND "
                       "questionId = %5").arg((int)isRight)
                                         .arg((int) inLastExam)
                                         .arg(userId)
                                         .arg(currentTheme)
                                         .arg(QString::number(slideNumber));

        if(query.exec(select))
        {
            query.next();
            //если валидная - апдейт
            if (query.isValid())
            {
                query.exec(update);
            }
            //если нет - инсерт
            else
            {
                query.exec(insert);
            }
        }

    }

    for (int i = 0; i < bottomSlides->length(); i++)
    {
        int slideNumber = bottomSlides->at(i).id;
        bool isRight = false;
        bool inLastExam = false;
        if (bottomExamQuestions.contains(i))
        {
            inLastExam = true;
            isRight = bottomExamQuestions.value(i);
        }
        //запрос по данному слайду из таблицы
        QString select = QString("SELECT time FROM questions WHERE userId = '%1' AND "
                                     "theme = '%2' AND "
                                     "questionId = %3").arg(userId).arg(currentTheme).arg(QString::number(slideNumber));
        QString insert = QString("INSERT INTO questions "
                "(theme, "
                "userId, "
                "questionId, "
                "time, "
                "inLastExam, "
                "isRight) "
                "VALUES ('%1', '%2', %3, %4, %5, %6)").arg(currentTheme)
                                                  .arg(userId)
                                                  .arg(QString::number(slideNumber))
                                                  .arg(QString::number(0))
                                                  .arg(inLastExam)
                                                  .arg(isRight);

        QString update = QString("UPDATE questions "
                       "SET isRight = %1, "
                       "inLastExam = %2 "
                       "WHERE userId = '%3' AND "
                       "theme = '%4' AND "
                       "questionId = %5").arg((int)isRight)
                                         .arg((int) inLastExam)
                                         .arg(userId)
                                         .arg(currentTheme)
                                         .arg(QString::number(slideNumber));

        if(query.exec(select))
        {
            query.next();
            //если валидная - апдейт
            if (query.isValid())
            {
                query.exec(update);
            }
            //если нет - инсерт
            else
            {
                query.exec(insert);
            }
        }


    }

    this->close();


}
