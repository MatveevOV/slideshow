#ifndef MODEDIALOG_H
#define MODEDIALOG_H

#include <QDialog>
#include <QDebug>
#include <QDir>
#include <QKeyEvent>

#include "logindialog.h"
#include "themeselectdialog.h"

class LoginDialog;

namespace Ui {
class ModeDialog;
}

class ModeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ModeDialog(LoginDialog *parent = 0);
    ~ModeDialog();

    enum Mode {TRAINING, EXAM, REPORT};

    void keyPressEvent(QKeyEvent *event);

    LoginDialog *getParent() const;

public slots:


private slots:
    void on_toTrainingButton_clicked();

    void on_toExamButton_clicked();

    void on_toReportButton_clicked();

    void on_toLoginButton_clicked();

    void reject();

private:
    Ui::ModeDialog *ui;
    LoginDialog *parent;


};

#endif // MODEDIALOG_H
