#ifndef DUMMYDIALOG_H
#define DUMMYDIALOG_H

#include <QDialog>
#include <QDebug>
#include <QTime>

#include "structures.h"
#include "themeselectdialog.h"
namespace Ui {
class DummyDialog;
}

class DummyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DummyDialog(QVector<Slide> *topSlides,
                         QVector<Slide> *bottomSlides,
                         ThemeSelectDialog *parent = 0);
    ~DummyDialog();



private slots:

private:
    Ui::DummyDialog *ui;
    QVector<Slide> *topSlides;
    QVector<Slide> *bottomSlides;
    int currentTopSlide, currentBottomSlide;
    ThemeSelectDialog *parent;
    QString userId;
    QString currentTheme;
    QTime topTime, bottomTime;

public slots:
    void setTopSlide(int i);
    void setBottomSlide(int i);
    void reject();



};

#endif // DUMMYDIALOG_H
