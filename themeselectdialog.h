#ifndef THEMESELECTDIALOG_H
#define THEMESELECTDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QBoxLayout>
#include <QEvent>
#include <QXmlStreamReader>
#include <QMessageBox>
#include <QKeyEvent>
#include <QDesktopWidget>
#include <QApplication>

#include "modedialog.h"
#include "examdialog.h"
#include "trainingdialog.h"
#include "reportdialog.h"
#include "structures.h"
#include "dummydialog.h"

class ModeDialog;

class ThemeSelectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ThemeSelectDialog(QStringList themes, int mode, ModeDialog *parent = 0);
    ~ThemeSelectDialog();


    QVector<Slide> topSlides;
    QVector<Slide> bottomSlides;

    bool themeParse(QString dir);
    Slide slideParse();




    ModeDialog *getParent() const;

    QString getSelectedTheme() const;

    int getExamNumberOfQuestion() const;

    void keyPressEvent(QKeyEvent *event);


private slots:
    void toModeDialog();
    void sendSelectedTheme();

private:
    ModeDialog *parent;
    int mode;
    QString selectedTheme;
    int examNumberOfQuestion;

    QXmlStreamReader Rxml;

    void reject();

};

#endif // THEMESELECTDIALOG_H
