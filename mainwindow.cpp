#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    LoginDialog *dialog = new LoginDialog();
    connect(dialog, &LoginDialog::sendUserData, this, &MainWindow::loginUser);
    if(!dialog->exec())
    {
         close();
    }

    createConnection();

    QSqlQuery query;
    QString str("create table person "
                "(id integer primary key, "
                "firstname varchar(20), "
                "lastname varchar(30), "
                "age integer)");
    if (!query.exec(str))
    {
        qDebug() << "Unable to create a table";
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    Slide *newSlide = new Slide("./Нормальное глазное дно");
    newSlide->showMaximized();
}


void MainWindow::on_pushButton_2_clicked()
{
    Slide *newSlide = new Slide("./Миопия и миопическая болезнь");
    newSlide->showMaximized();
}

void MainWindow::on_pushButton_3_clicked()
{
    Slide *newSlide = new Slide("./Диабетическая ретинопатия");
    newSlide->showMaximized();
}

void MainWindow::on_pushButton_4_clicked()
{
    Slide *newSlide = new Slide("./Возрастная макулярная дегенеразация");
    newSlide->showMaximized();
}

void MainWindow::on_pushButton_5_clicked()
{
    Slide *newSlide = new Slide("./Окклюзия вен сетчатки");
    newSlide->showMaximized();
}

void MainWindow::loginUser(QString secondName, QString firstName, QString patronymic, QString university, QString group)
{

}

bool MainWindow::createConnection()
{

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("Ophthalmoscopy");
    if (!db.open())
    {
        qDebug() << "false";
        return false;
    }
    return true;
}
