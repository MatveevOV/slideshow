#ifndef TRAININGDIALOG_H
#define TRAININGDIALOG_H

#include <QDialog>
#include <QDebug>
#include <QTime>
#include <QResizeEvent>
#include <QMouseEvent>

#include "structures.h"
#include "themeselectdialog.h"


class ThemeSelectDialog;

namespace Ui {
class TrainingDialog;
}

class TrainingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TrainingDialog(QVector<Slide> *topSlides,
                            QVector<Slide> *bottomSlides,
                            ThemeSelectDialog *parent = 0);
    ~TrainingDialog();

private slots:
    void on_backButtonTop_clicked();
    void on_forwardButtonTop_clicked();
    void on_backButtonBottom_clicked();
    void on_forwardButtonBottom_clicked();

    void on_endButton_clicked();

private:
    Ui::TrainingDialog *ui;
    QVector<Slide> *topSlides;
    QVector<Slide> *bottomSlides;
    int currentTopSlide, currentBottomSlide;
    ThemeSelectDialog *parent;
    QString userId;
    QString currentTheme;
    QTime topTime, bottomTime;


    void reject();
    void setTopSlide(int i);
    void setBottomSlide(int i);
    bool updateSlideInDb(int slideNumber, int secs);

    void resizeEvent(QResizeEvent * event);
    void mousePressEvent(QMouseEvent *e);

signals:
    void topEyeChanged(int i);
    void bottomEyeChanged(int i);


};

#endif // TRAININGDIALOG_H
