#ifndef EXAMDIALOG_H
#define EXAMDIALOG_H

#include <QDialog>
#include <QRadioButton>
#include <QBoxLayout>
#include <QCryptographicHash>
#include <QDebug>
#include <QSqlRecord>
#include <QResizeEvent>

#include "structures.h"
#include "themeselectdialog.h"

class ThemeSelectDialog;


namespace Ui {
class ExamDialog;
}

class ExamDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ExamDialog(QVector<Slide> *topSlides,
                        QVector<Slide> *bottomSlides,
                        ThemeSelectDialog *parent = 0);
    ~ExamDialog();

private slots:
    void on_backButton_clicked();    
    void on_forwardButton_clicked();
    QMap<int, bool> getTopSlidesForExam();
    QMap<int, bool> getBottomSlidesForExam();
    bool checkTopSlide(QString answer, int slideNumber);
    bool checkBottomSlide(QString answer, int slideNumber);


    void on_endButton_clicked();

private:
    Ui::ExamDialog *ui;
    ThemeSelectDialog *parent;
    QVector<Slide> *topSlides;
    QVector<Slide> *bottomSlides;
    QMap<int, bool>::iterator currentTopSlide, currentBottomSlide;
    QVBoxLayout topVBox, bottomVBox;

    QString userId, currentTheme;
    int numberOfQuestions, currentQuestion;

    QMap<int, bool> topExamQuestions;
    QMap<int, bool> bottomExamQuestions;

    void reject();
    void setSlide(int t, int b);

    void resizeEvent(QResizeEvent * event);

signals:
    void topEyeChanged(int i);
    void bottomEyeChanged(int i);

};

#endif // EXAMDIALOG_H
