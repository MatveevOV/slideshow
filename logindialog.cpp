#include "logindialog.h"
#include "ui_logindialog.h"

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    connect(ui->loginButton, &QPushButton::released, this, &LoginDialog::accept);
    //connect(ui->exitButton, &QPushButton::released, this, &LoginDialog::reject);

    setWindowFlags(Qt::WindowCloseButtonHint);


    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("ophthalmoscopy");
    db.open();

    QRegExp nameREX;
    nameREX.setPattern("[A-ZА-Я-0-9]{0,}");
    nameREX.setCaseSensitivity(Qt::CaseInsensitive);
    nameREX.setPatternSyntax(QRegExp::RegExp);

    ui->firstNameEdit->setValidator(new QRegExpValidator(nameREX));
    ui->secondNameEdit->setValidator(new QRegExpValidator(nameREX));
    ui->patronymicEdit->setValidator(new QRegExpValidator(nameREX));
    ui->universityEdit->setValidator(new QRegExpValidator(nameREX));
    ui->groupEdit->setValidator(new QRegExpValidator(nameREX));



}

LoginDialog::~LoginDialog()
{
    delete ui;
}

User LoginDialog::getCurrentUser() const
{
    return currentUser;
}

void LoginDialog::accept()
{

    QString errorString;

    if (ui->secondNameEdit->text().isEmpty())
    {
        errorString.append("- Фамилия\n");
    }
    if (ui->firstNameEdit->text().isEmpty())
    {
        errorString.append("- Имя\n");
    }
    if (ui->patronymicEdit->text().isEmpty())
    {
        errorString.append("- Отчество\n");
    }
    if (ui->universityEdit->text().isEmpty())
    {
        errorString.append("- Название учебного заведения\n");
    }
    if (ui->groupEdit->text().isEmpty())
    {
        errorString.append("- Группа\n");
    }

    if (!errorString.isEmpty())
    {
        errorString.prepend("Заполните следующие поля:\n");

        QMessageBox msgBox;
        msgBox.setWindowTitle("Ошибка ввода");
        msgBox.setText(errorString);
        msgBox.exec();
        return;
    }



    hide();

    QSqlQuery query(db);

    //создание таблицы, если нужно

    QString str =     "create table if not exists users "
              "(userId text primary key, "
              "firstName text not null, "
              "secondName text not null, "
              "patronymic text not null, "
              "university text not null, "
              "class text not null)";



    qDebug() << "create table users" <<query.exec(str);
    //-------


    currentUser.firstName = ui->firstNameEdit->text();
    currentUser.secondName = ui->secondNameEdit->text();
    currentUser.patronymic = ui->patronymicEdit->text();
    currentUser.university = ui->universityEdit->text();
    currentUser.group = ui->groupEdit->text();

    //получение id пользователя, если он уже входил

    QString selectId = QString("SELECT userId FROM users WHERE firstName = '%1' AND "
                               "secondName = '%2' AND patronymic = '%3' AND university = '%4' AND "
                               "class = '%5'").arg(currentUser.firstName).arg(currentUser.secondName)
                                            .arg(currentUser.patronymic).arg(currentUser.university)
                                            .arg(currentUser.group);



    QString userId;
    bool selectBool = query.exec(selectId);
    qDebug() << selectBool << query.lastError()  << query.lastQuery();
    if(selectBool)
    {
        query.next();
        userId = query.value(0).toString();
        qDebug() << "user" << userId <<  query.lastError();
        currentUser.uuid = userId;
    }
    if(userId.isEmpty())
    {

        query.prepare("INSERT INTO users VALUES (?, ?, ?, ?, ?, ?)");
        currentUser.uuid = QUuid::createUuid().toString();
        query.addBindValue(currentUser.uuid);
        query.addBindValue(currentUser.firstName);
        query.addBindValue(currentUser.secondName);
        query.addBindValue(currentUser.patronymic);
        query.addBindValue(currentUser.university);
        query.addBindValue(currentUser.group);
        qDebug() << "insert" << query.exec() << query.lastError();
    }





    modeDialog = new ModeDialog(this);
    modeDialog->show();
}

void LoginDialog::reject()
{

    done(Rejected);
}


