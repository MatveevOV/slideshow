#ifndef REPORTDIALOG_H
#define REPORTDIALOG_H

#include <QDialog>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QTableView>
#include <QTreeView>
#include <QScroller>
#include <QTableWidget>
#include <QTableWidgetItem>

#include "themeselectdialog.h"


class ThemeSelectDialog;

namespace Ui {
class ReportDialog;
}

class ReportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ReportDialog(QVector<Slide> *topSlides,
                          QVector<Slide> *bottomSlides,
                          ThemeSelectDialog *parent = 0);
    ~ReportDialog();

private slots:
    void on_backButton_clicked();

private:
    Ui::ReportDialog *ui;
    ThemeSelectDialog *parent;
    QString userId;
    QString currentTheme;
    QVector<Slide> *topSlides;
    QVector<Slide> *bottomSlides;

    void reject();
};

#endif // REPORTDIALOG_H
