#ifndef SLIDE_H
#define SLIDE_H

#include <QDialog>
#include <QDesktopWidget>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QFileInfoList>
#include <QImage>
#include <QTimer>
#include <QKeyEvent>

namespace Ui {
class Slide;
}

class Slide : public QDialog
{
    Q_OBJECT

public:
    explicit Slide(QString dirName, QWidget *parent = 0);
    ~Slide();

private slots:
    void on_leftButton_clicked();
    void on_rightButton_clicked();

private:
    Ui::Slide *ui;
    QFileInfoList showedFiles;
    int currentPic;
    int pixCount;

    void keyPressEvent(QKeyEvent *event);
};

#endif // SLIDE_H
