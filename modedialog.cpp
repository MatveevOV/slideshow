#include "modedialog.h"
#include "ui_modedialog.h"

ModeDialog::ModeDialog(LoginDialog *parent) :
    ui(new Ui::ModeDialog)
{
    ui->setupUi(this);
    this->parent = parent;
    setWindowFlags(Qt::Dialog|Qt::WindowCloseButtonHint);

}

ModeDialog::~ModeDialog()
{
    delete ui;
}

void ModeDialog::on_toTrainingButton_clicked()
{

    QDir dir("./data");
    QStringList themes = dir.entryList(QDir::AllDirs|QDir::NoDotAndDotDot);
    ThemeSelectDialog *d = new ThemeSelectDialog(themes,TRAINING, this);
    d->setWindowTitle("Выбор темы для обучения");

    hide();
    d->show();
}

void ModeDialog::on_toExamButton_clicked()
{

    QDir dir("./data");
    QStringList themes = dir.entryList(QDir::AllDirs|QDir::NoDotAndDotDot);
    ThemeSelectDialog *d = new ThemeSelectDialog(themes, EXAM, this);
    d->setWindowTitle("Выбор темы для экзамена");

    hide();
    d->show();
}

void ModeDialog::on_toReportButton_clicked()
{
    QDir dir("./data");
    QStringList themes = dir.entryList(QDir::AllDirs|QDir::NoDotAndDotDot);
    ThemeSelectDialog *d = new ThemeSelectDialog(themes,REPORT, this);
    d->setWindowTitle("Выбор темы для отчета");

    hide();
    d->show();
}

void ModeDialog::on_toLoginButton_clicked()
{
    this->parent->show();
    hide();

}

void ModeDialog::reject()
{

    done(Rejected);

}

LoginDialog *ModeDialog::getParent() const
{
    return parent;
}

void ModeDialog::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
    {
        on_toLoginButton_clicked();
    }

}
